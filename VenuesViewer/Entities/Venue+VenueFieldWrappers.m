//
//  Venue+VenueFieldWrappers.m
//  VenuesViewer
//
//  Created by Admin on 30.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Venue+VenueFieldWrappers.h"

#import <objc/runtime.h>

@implementation VenueFieldWrapper

+ (VenueFieldWrapper *)wrapperWithName:(NSString *)name value:(NSString *)value {
    VenueFieldWrapper *newWrapper = [[VenueFieldWrapper alloc] init];
    newWrapper.name = name;
    newWrapper.value = value;
    return newWrapper;
}

@end

@implementation Venue (VenueFieldWrappers)

- (NSArray *)venueFieldWrappers {
    VenueFieldWrapper *nameWraper = [VenueFieldWrapper wrapperWithName:NSLocalizedString(@"Name: ", nil)
                                                                 value:self.name];
    VenueFieldWrapper *distanceWrapper = [VenueFieldWrapper wrapperWithName:NSLocalizedString(@"Distance: ", nil)
                                                                      value:self.userRepresentationDistance];
    VenueFieldWrapper *addressWrapper = [VenueFieldWrapper wrapperWithName:NSLocalizedString(@"Address: ", nil)
                                                                     value:self.address];
    VenueFieldWrapper *primaryCategoryWrapper = [VenueFieldWrapper wrapperWithName:NSLocalizedString(@"Primary category: ", nil)
                                                                             value:self.primaryCategoryName];
    return @[nameWraper, distanceWrapper, addressWrapper, primaryCategoryWrapper];
}

@end
