//
//  VenueCategory.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenueCategory.h"

#import <RestKit/RestKit.h>

@implementation VenueCategory

+ (RKObjectMapping *)objectMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{@"id" : @"categoryId",
                                                  @"name" : @"name",
                                                  @"primary" : @"primary"}];
    return mapping;
}

@end
