//
//  Venue.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Venue.h"
#import "VenueCategory.h"

#import <RestKit/RestKit.h>

NSUInteger const OneKilometer = 1000;
NSUInteger const OneTousandKilometers = OneKilometer * 1000;

@implementation Venue

#pragma mark - Accessors

- (void)setDistance:(NSNumber *)distance {
    _distance = [distance copy];
    [self updateUserRepresentationDistance];
}

- (void)setCategories:(NSSet *)categories {
    _categories = [categories copy];
    [self updatePrimaryCategoryName];
}

#pragma mark - Public Methods

+ (RKObjectMapping *)objectMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    [mapping addAttributeMappingsFromDictionary:@{@"id" : @"venueId",
                                                  @"name" : @"name",
                                                  @"location.distance" : @"distance",
                                                  @"location.address" : @"address",
                                                  @"location.lat" : @"latitude",
                                                  @"location.lng" : @"longitude"}];
    RKRelationshipMapping *relationshipMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"categories"
                                                                                             toKeyPath:@"categories"
                                                                                           withMapping:[VenueCategory objectMapping]];
    [mapping addPropertyMapping:relationshipMapping];
    return mapping;
}

#pragma mark - Private Methods

- (void)updateUserRepresentationDistance {
    NSUInteger metersDisntace = [self.distance unsignedIntegerValue];
    if (metersDisntace < OneKilometer) {
        _userRepresentationDistance = [NSString stringWithFormat:@"%tu %@", metersDisntace, NSLocalizedString(@"m", nil)];
    }
    else if (metersDisntace < OneTousandKilometers) {
        NSUInteger kilometersDisntace = metersDisntace / OneKilometer;
        _userRepresentationDistance = [NSString stringWithFormat:@"%tu %@", kilometersDisntace, NSLocalizedString(@"km", nil)];
    }
    else {
        NSUInteger thousandKilometeresDistance = metersDisntace / OneTousandKilometers;
        _userRepresentationDistance = [NSString stringWithFormat:@"%tu %@", thousandKilometeresDistance, NSLocalizedString(@"thousands km", nil)];
    }
}

- (void)updatePrimaryCategoryName {
    NSPredicate *primaryCategoryPredicate = [NSPredicate predicateWithFormat:@"primary = TRUE"];
    NSSet *primaryCategories = [self.categories filteredSetUsingPredicate:primaryCategoryPredicate];
    _primaryCategoryName = [primaryCategories.anyObject valueForKey:@"name"];
}

@end
