//
//  Venue.h
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKObjectMapping;

extern NSUInteger const OneKilometer;
extern NSUInteger const OneTousandKilometers;

@interface Venue : NSObject

@property (nonatomic, copy) NSString *venueId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *distance;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSNumber *latitude;
@property (nonatomic, copy) NSNumber *longitude;

@property (nonatomic, copy) NSSet *categories;

@property (nonatomic, readonly) NSString *userRepresentationDistance;
@property (nonatomic, readonly) NSString *primaryCategoryName;

+ (RKObjectMapping *)objectMapping;

@end
