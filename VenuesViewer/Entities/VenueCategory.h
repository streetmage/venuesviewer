//
//  VenueCategory.h
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKObjectMapping;

@interface VenueCategory : NSObject

@property (nonatomic, copy) NSString *categoryId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *primary;

+ (RKObjectMapping *)objectMapping;

@end
