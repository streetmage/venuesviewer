//
//  Venue+VenueFieldWrappers.h
//  VenuesViewer
//
//  Created by Admin on 30.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Venue.h"

@interface VenueFieldWrapper : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *value;

+ (VenueFieldWrapper *)wrapperWithName:(NSString *)name value:(NSString *)value;

@end

@interface Venue (VenueFieldWrappers)

@property (nonatomic, readonly) NSArray *venueFieldWrappers;

@end
