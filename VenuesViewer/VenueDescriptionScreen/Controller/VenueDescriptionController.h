//
//  VenueDescriptionController.h
//  VenuesViewer
//
//  Created by Admin on 30.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Venue;

@interface VenueDescriptionController : UITableViewController

@property (nonatomic, strong) Venue *venue;

- (instancetype)initWithVenue:(Venue *)venue;

@end
