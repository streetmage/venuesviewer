//
//  VenueDescriptionController.m
//  VenuesViewer
//
//  Created by Admin on 30.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenueDescriptionController.h"
#import "Venue+VenueFieldWrappers.h"

@interface VenueDescriptionController ()

@property (nonatomic, copy) NSArray *venueFieldWrappers;

@end

@implementation VenueDescriptionController

- (instancetype)initWithVenue:(Venue *)venue {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
        self.venue = venue;
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Description", nil);
}

#pragma mark - Accessors

- (void)setVenue:(Venue *)venue {
    _venue = venue;
    [self reloadData];
}

#pragma mark - Private Methods

- (void)reloadData {
    self.venueFieldWrappers = [self.venue venueFieldWrappers];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.venueFieldWrappers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *venueDescriptionCellId = @"venueDescriptionCellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:venueDescriptionCellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:venueDescriptionCellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    VenueFieldWrapper *wrapper = [self.venueFieldWrappers objectAtIndex:indexPath.row];
    cell.textLabel.text = wrapper.name;
    cell.detailTextLabel.text = wrapper.value;
    return cell;
}

@end
