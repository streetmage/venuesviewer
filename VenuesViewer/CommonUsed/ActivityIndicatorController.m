//
//  ActivityIndicatorController.m
//  VenuesViewer
//
//  Created by Admin on 30.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ActivityIndicatorController.h"

@interface ActivityIndicatorController ()

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation ActivityIndicatorController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.activityIndicator setCenter:self.view.center];
}


#pragma mark - Accessors

- (UIActivityIndicatorView *)activityIndicator {
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_activityIndicator setColor:[UIColor grayColor]];
    }
    if (!_activityIndicator.superview) {
        [self.view addSubview:_activityIndicator];
        [self.view bringSubviewToFront:_activityIndicator];
    }
    return _activityIndicator;
}

#pragma mark - Public Methods

- (void)showActivityIndicator {
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator {
    [self.activityIndicator stopAnimating];
}

@end
