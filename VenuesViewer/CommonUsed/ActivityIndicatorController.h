//
//  ActivityIndicatorController.h
//  VenuesViewer
//
//  Created by Admin on 30.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityIndicatorController : UIViewController

- (void)showActivityIndicator;
- (void)hideActivityIndicator;

@end
