//
//  VenuesListController.m
//  VenuesViewer
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenuesListController.h"
#import "VenuesSearchModel.h"
#import "VenuesListView.h"
#import "VenuesListCell.h"

#import "VenueDescriptionController.h"

@interface VenuesListController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) VenuesListView *screenView;

@end

@implementation VenuesListController

- (void)loadView {
    _screenView = [[VenuesListView alloc] init];
    self.view = _screenView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"List", nil);
    [self.screenView.tableView setDataSource:self];
    [self.screenView.tableView setDelegate:self];
}

#pragma mark - VenuesSearchBaseControllerDataReload Methods

- (void)reloadData {
    [self.screenView.tableView reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.venues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *venuesListCellId = @"VenuesListCellId";
    VenuesListCell *cell = [tableView dequeueReusableCellWithIdentifier:venuesListCellId];
    if (!cell) {
        cell = [[VenuesListCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                     reuseIdentifier:venuesListCellId];
    }
    Venue *venue = [self.model.venues objectAtIndex:indexPath.row];
    [cell setupCellWithVenueName:venue.name
                    categoryName:venue.primaryCategoryName
                        distance:venue.userRepresentationDistance];
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [VenuesListCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Venue *venue = [self.model.venues objectAtIndex:indexPath.row];
    VenueDescriptionController *descriptionController = [[VenueDescriptionController alloc] initWithVenue:venue];
    [self.navigationController pushViewController:descriptionController animated:YES];
}

@end
