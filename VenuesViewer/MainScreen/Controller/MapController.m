//
//  MapController.m
//  VenuesViewer
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "MapController.h"
#import "MapModel.h"
#import "MapView.h"

@interface MapController () <MKMapViewDelegate>

// Data
@property (nonatomic, strong) MapModel *model;

// View
@property (nonatomic, strong) MapView *screenView;

@end

@implementation MapController

@synthesize model = _model;

- (void)loadView {
    _screenView = [[MapView alloc] init];
    self.view = _screenView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Map", nil);
}

#pragma mark - Accessors

- (VenuesSearchModel *)model {
    if (!_model) {
        _model = [[MapModel alloc] init];
    }
    return _model;
}

#pragma mark - Private Methods

- (void)reloadMapData {
    [self removeAllPins];
    [self.screenView.mapView addAnnotations:self.model.annotations];
}

- (void)removeAllPins {
    id userLocation = [self.screenView.mapView userLocation];
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:self.screenView.mapView.annotations];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation];
    }
    [self.screenView.mapView removeAnnotations:pins];
}

#pragma mark - VenuesSearchBaseControllerDataReload Methods

- (void)reloadData {
    CLLocationCoordinate2D currentCoordinate = self.model.currentCoordinate;
    CLLocationDistance currentDistance = self.model.currentRadius * 2; // as we have distance more like diameter
                                                                       // than radius
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(currentCoordinate, currentDistance, currentDistance);
    [self.screenView.mapView setRegion:region animated:YES];
    [self reloadMapData];
}

#pragma mark - MKMapViewDelegate Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *annotationViewReuseId = @"AnnotationViewReuseId";
    if ([annotation isKindOfClass:[MapAnnotation class]]) {
        MKAnnotationView *annotationView = [self.screenView.mapView dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseId];
        if (annotationView != nil) {
            [annotationView setAnnotation:annotation];
            
        }
        else {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:annotationViewReuseId];
            annotationView.enabled = NO;
            annotationView.canShowCallout = NO;
        }
        return annotationView;
    }
    return nil;
}

@end
