//
//  VenuesSearchBaseController.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenuesSearchBaseController.h"
#import "VenuesSearchModel.h"

@interface VenuesSearchBaseController ()

@property (nonatomic, readwrite) VenuesSearchModel *model;

@end

@implementation VenuesSearchBaseController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupModel];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.model startLocationChangeEventsPropagation];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.model stopLocationChangeEventsPropagation];
}

#pragma mark - Accessors

- (VenuesSearchModel *)model {
    if (!_model) {
        _model = [[VenuesSearchModel alloc] init];
    }
    return _model;
}

#pragma mark - Private Methods

- (void)setupModel {
    
    __weak typeof(self) weakSelf = self;
    
    [self.model setLocationChangeHandler:^{
        [weakSelf showActivityIndicator];
        [weakSelf.model loadVenuesWithCompletion:^(BOOL success, NSError *error) {
            [weakSelf hideActivityIndicator];
            if ([weakSelf respondsToSelector:@selector(reloadData)]) {
                [weakSelf performSelector:@selector(reloadData)];
            }
        }];
    }];
    
    [self.model setAccessDeniedErrorHandler:^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention!", nil)
                                                            message:NSLocalizedString(@"Geolocation is not permitted for this app. Please, allow it in the Settings.", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    
}

@end
