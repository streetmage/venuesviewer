//
//  MainTabBarController.m
//  VenuesViewer
//
//  Created by Admin on 27.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "MainTabBarController.h"

#import "LoginScreenController.h"
#import "VenuesListController.h"
#import "MapController.h"

#import "AppSession.h"

@implementation MainTabBarController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        VenuesListController *venuesListController = [[VenuesListController alloc] init];
        UINavigationController *venuesListNavigationController = [[UINavigationController alloc] initWithRootViewController:venuesListController];
        UITabBarItem *venuesListTabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"List", nil)
                                                                           image:[UIImage imageNamed:@"selected_list.png"]
                                                                   selectedImage:[UIImage imageNamed:@"unselected_list.png"]];
        [venuesListNavigationController setTabBarItem:venuesListTabBarItem];
        
        MapController *mapController = [[MapController alloc] init];
        UINavigationController *mapNavigationController = [[UINavigationController alloc] initWithRootViewController:mapController];
        UITabBarItem *mapTabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Map", nil)
                                                                    image:[UIImage imageNamed:@"selected_map.png"]
                                                            selectedImage:[UIImage imageNamed:@"unselected_map.png"]];
        [mapNavigationController setTabBarItem:mapTabBarItem];
        
        [self setViewControllers:@[venuesListNavigationController, mapNavigationController]];
        
    }
    
    return self;
}

@end
