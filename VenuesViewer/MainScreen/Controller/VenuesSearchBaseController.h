//
//  VenuesSearchBaseController.h
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ActivityIndicatorController.h"

@class VenuesSearchModel;

@protocol VenuesSearchBaseControllerDataReload <NSObject>

@required

- (void)reloadData;

@end

@interface VenuesSearchBaseController : ActivityIndicatorController

@property (nonatomic, readonly) VenuesSearchModel *model;

@end
