//
//  VenuesListController.h
//  VenuesViewer
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenuesSearchBaseController.h"

@interface VenuesListController : VenuesSearchBaseController <VenuesSearchBaseControllerDataReload>

@end
