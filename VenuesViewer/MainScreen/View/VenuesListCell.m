//
//  VenuesListCell.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenuesListCell.h"

static CGFloat const VenuesListCellHeight = 50.0f;
static CGFloat const DistanceLabelMaxWidth = 80.0f;

@interface VenuesListCell ()

@property (nonatomic, strong) UILabel *distanceLabel;

@end

@implementation VenuesListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        _distanceLabel = [[UILabel alloc] init];
        [_distanceLabel setFont:[UIFont systemFontOfSize:12.0f]];
        [self addSubview:_distanceLabel];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIEdgeInsets cellPadding = UIEdgeInsetsMake(5.0f, 15.0f, 5.0f, 5.0f);
    
    CGSize distanceLabelSize = [_distanceLabel sizeThatFits:CGSizeMake(DistanceLabelMaxWidth, MAXFLOAT)];
    [_distanceLabel setFrame:CGRectMake(self.bounds.size.width - distanceLabelSize.width - cellPadding.right,
                                        cellPadding.top,
                                        distanceLabelSize.width,
                                        distanceLabelSize.height)];
    
    CGFloat textLabelAvailableWidth = self.frame.size.width - distanceLabelSize.width - cellPadding.left - cellPadding.right;
    CGSize textLabelSize = [self.textLabel sizeThatFits:CGSizeMake(textLabelAvailableWidth, MAXFLOAT)];
    [self.textLabel setFrame:CGRectMake(cellPadding.left,
                                        cellPadding.top,
                                        textLabelAvailableWidth > textLabelSize.width ? textLabelSize.width : textLabelAvailableWidth,
                                        textLabelSize.height)];
    
    CGSize detailTextLabelSize = [self.detailTextLabel sizeThatFits:textLabelSize];
    [self.detailTextLabel setFrame:CGRectMake(self.textLabel.frame.origin.x,
                                              CGRectGetMaxY(self.textLabel.frame),
                                              textLabelAvailableWidth > detailTextLabelSize.width ? detailTextLabelSize.width : textLabelAvailableWidth,
                                              detailTextLabelSize.height)];
    
}

#pragma mark - Public Methods

+ (CGFloat)cellHeight {
    return VenuesListCellHeight;
}

- (void)setupCellWithVenueName:(NSString *)venueName
                  categoryName:(NSString *)categoryName
                      distance:(NSString *)distance {
    self.textLabel.text = venueName;
    self.detailTextLabel.text = categoryName;
    self.distanceLabel.text = distance;
}

@end
