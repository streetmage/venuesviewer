//
//  VenuesListCell.h
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenuesListCell : UITableViewCell

+ (CGFloat)cellHeight;

- (void)setupCellWithVenueName:(NSString *)venueName
                  categoryName:(NSString *)categoryName
                      distance:(NSString *)distance;

@end
