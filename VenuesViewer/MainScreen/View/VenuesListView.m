//
//  VenuesListView.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenuesListView.h"

@implementation VenuesListView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        _tableView = [[UITableView alloc] init];
        [self addSubview:_tableView];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_tableView setFrame:self.bounds];
}

@end
