//
//  VenuesListView.h
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenuesListView : UIView

@property (nonatomic, readonly) UITableView *tableView;

@end
