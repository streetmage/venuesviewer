//
//  MapView.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "MapView.h"

@implementation MapView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        _mapView = [[MKMapView alloc] init];
        [self addSubview:_mapView];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_mapView setFrame:self.bounds];
}

@end
