//
//  MapModel.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "MapModel.h"

@implementation MapModel

#pragma mark - Public Methods

- (void)loadVenuesWithCompletion:(void (^)(BOOL, NSError *))completion {
    __weak typeof(self) weakSelf = self;
    [super loadVenuesWithCompletion:^(BOOL success, NSError *error) {
        [weakSelf setupAnnotations];
        if (completion) {
            completion(success, error);
        }
    }];
}

#pragma mark - Private Methods

- (void)setupAnnotations {
    NSMutableArray *mutableAnnotation = [NSMutableArray array];
    for (Venue *venue in self.venues) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([venue.latitude doubleValue],
                                                                       [venue.longitude doubleValue]);
        MapAnnotation *annotation = [[MapAnnotation alloc] initWithTitle:venue.name
                                                                subtitle:venue.address
                                                              coordinate:coordinate];
        [mutableAnnotation addObject:annotation];
    }
    _annotations = [NSArray arrayWithArray:mutableAnnotation];
}

@end
