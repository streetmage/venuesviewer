//
//  VenuesSearchModel.h
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Venue.h"

typedef void(^VenuesSearchModelHandler)();

@interface VenuesSearchModel : NSObject

@property (nonatomic, readonly) NSArray *venues;

@property (nonatomic, assign) CLLocationDistance currentRadius;
@property (nonatomic, readonly) CLLocationCoordinate2D currentCoordinate;

@property (nonatomic, strong) VenuesSearchModelHandler locationChangeHandler;
@property (nonatomic, strong) VenuesSearchModelHandler accessDeniedErrorHandler;

- (void)startLocationChangeEventsPropagation;
- (void)stopLocationChangeEventsPropagation;

- (void)loadVenuesWithCompletion:(void(^)(BOOL success, NSError *error))completion;

@end
