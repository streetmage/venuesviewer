//
//  VenuesSearchModel.m
//  VenuesViewer
//
//  Created by Admin on 29.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VenuesSearchModel.h"
#import "AppService.h"

static CLLocationDistance const LocationUpdateDistance = 100;

@interface VenuesSearchModel () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *cachedLocation;

@end

@implementation VenuesSearchModel

- (instancetype)init {
    self = [super init];
    
    if (self) {
        
        _currentRadius = OneKilometer;
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = LocationUpdateDistance;
        
    }
    
    return self;
}

#pragma mark - Accessors

- (CLLocationCoordinate2D)currentCoordinate {
    return self.cachedLocation.coordinate;
}

#pragma mark - Public Methods

- (void)startLocationChangeEventsPropagation {
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [_locationManager startUpdatingLocation];
}

- (void)stopLocationChangeEventsPropagation {
    [_locationManager stopUpdatingLocation];
}

- (void)loadVenuesWithCompletion:(void(^)(BOOL success, NSError *error))completion {
    CLLocationCoordinate2D coordinate = self.cachedLocation.coordinate;
    __weak typeof(self) weakSelf = self;
    [[AppService sharedService] cancelSearch];
    [[AppService sharedService] searchVenuesWithLatitude:coordinate.latitude
                                               longitude:coordinate.longitude
                                                  radius:self.currentRadius
                                              completion:^(BOOL success, NSArray *venues, NSString *responseString, NSError *error) {
                                                  if (completion) {
                                                      [weakSelf setValue:[venues copy] forKey:@"_venues"];
                                                      if (completion) {
                                                          completion(success, error);
                                                      }
                                                  }
                                              }];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject];
    CLLocationDistance locationDistance = [currentLocation distanceFromLocation:self.cachedLocation];
    if (self.cachedLocation == nil || locationDistance >= LocationUpdateDistance) {
        self.cachedLocation = [currentLocation copy];
        if (self.locationChangeHandler) {
            self.locationChangeHandler();
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (error.code == kCLErrorDenied && self.accessDeniedErrorHandler) {
        self.accessDeniedErrorHandler();
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self startLocationChangeEventsPropagation];
    }
}

@end
