//
//  AppSession.m
//  VenuesViewer
//
//  Created by Admin on 24.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AppSession.h"

#import <SSKeychain/SSKeychain.h>

static NSString *const AuthorizedFlagKey = @"authorized";

@interface AppSession ()

@property (nonatomic, strong) NSString *serviceName;
@property (nonatomic, readonly) NSString *account;

// Authorized flag is used separately from access token to
// avoid passing login screen after app been reinstalled
@property (nonatomic, readwrite) BOOL authorized;

@end

@implementation AppSession

@synthesize accessToken = _accessToken;
@synthesize account = _account;

#pragma mark - Singleton Methods

+ (instancetype)defaultSession {
    static AppSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        session = [[AppSession alloc] init];
    });
    return session;
}

#pragma mark - Accessors

- (void)setAccessToken:(NSString *)accessToken {
    _accessToken = accessToken;
    if (accessToken.length > 0) {
        [SSKeychain setPassword:_accessToken
                     forService:self.serviceName
                        account:self.account];
        self.authorized = YES;
    }
}

- (NSString *)accessToken {
    if (!_accessToken) {
        _accessToken = [SSKeychain passwordForService:self.serviceName
                                              account:self.account];
    }
    return _accessToken;
}

- (NSString *)serviceName {
    if (!_serviceName) {
        _serviceName = [[NSBundle mainBundle] bundleIdentifier];
    }
    return _serviceName;
}

- (void)setAuthorized:(BOOL)authorized {
    [[NSUserDefaults standardUserDefaults] setObject:@(authorized) forKey:AuthorizedFlagKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isAuthorized {
    return [[NSUserDefaults standardUserDefaults] objectForKey:AuthorizedFlagKey] && self.accessToken.length > 0;
}

- (NSString *)account {
    if (!_account) {
        _account = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
    return _account;
}

#pragma mark - Public Methods

- (void)revokeToken {
    [SSKeychain deletePasswordForService:self.serviceName account:self.account];
}

@end
