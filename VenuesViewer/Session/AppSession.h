//
//  AppSession.h
//  VenuesViewer
//
//  Created by Admin on 24.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSession : NSObject

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, readonly, getter = isAuthorized) BOOL authorized;

+ (instancetype)defaultSession;

- (void)revokeToken;

@end
