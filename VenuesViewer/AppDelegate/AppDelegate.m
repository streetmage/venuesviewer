//
//  AppDelegate.m
//  VenuesViewer
//
//  Created by Admin on 22.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AppDelegate.h"

#import "MainTabBarController.h"
#import "LoginScreenController.h"

#import "AppServiceReachability.h"
#import "AppSession.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[AppService sharedService] startPopulatingReachabilityNotifications];
    
    // I wish I used presentViewConroller:animated:completion: for LoginScreenController
    // but in iOS 8 controller's presentation performs with delay
    UIViewController *rootViewController = nil;
    if (![AppSession defaultSession].isAuthorized) {
        rootViewController = [[LoginScreenController alloc] init];
    } else {
        rootViewController = [[MainTabBarController alloc] init];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = rootViewController;
    [self.window makeKeyAndVisible];
    
    return YES;
    
}

@end
