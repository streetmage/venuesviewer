//
//  LoginScreenController.m
//  VenuesViewer
//
//  Created by Admin on 22.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "LoginScreenController.h"
#import "LoginScreenModel.h"
#import "LoginScreenView.h"

#import "MainTabBarController.h"

#import "AppServiceReachability.h"

static NSInteger const InternetConnectionOfflineCode = -1009;

@interface LoginScreenController () <UIWebViewDelegate>

// Data
@property (nonatomic, strong) LoginScreenModel *model;
@property (nonatomic, assign) BOOL needsDataReload;

// View
@property (nonatomic, strong) LoginScreenView *screenView;

@end

@implementation LoginScreenController

#pragma mark - UIViewController Lifecycle

- (void)loadView {
    _screenView = [[LoginScreenView alloc] init];
    self.view = _screenView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Login", nil);
    
    [self.screenView.webView setDelegate:self];
    
    [self setNeedsDataReload:YES];
    [self reloadDataIfNeeded];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onApplicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onServiceReachabilityStatusChange:)
                                                 name:ServiceReachabilityChangeNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Accessors

- (LoginScreenModel *)model {
    if (!_model) {
        _model = [[LoginScreenModel alloc] init];
    }
    return _model;
}

#pragma mark - Private Methods

- (void)reloadDataIfNeeded {
    if (self.needsDataReload) {
        [self setNeedsDataReload:NO];
        [self.screenView.webView loadRequest:self.model.authenticateRequest];
    }
}

- (void)onApplicationDidBecomeActive:(NSNotification *)notification {
    [self reloadDataIfNeeded];
}

- (void)onServiceReachabilityStatusChange:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    ServiceReachabilityStatus reachabilityStatus = [[userInfo valueForKey:ServiceReachabilityStatusInfoKey] unsignedIntegerValue];
    if (reachabilityStatus == ServiceReachable) {
        [self reloadDataIfNeeded];
    }
}

- (void)showMainController {
    MainTabBarController *mainTabBarController = [[MainTabBarController alloc] init];
    [UIApplication sharedApplication].keyWindow.rootViewController = mainTabBarController;
}

- (void)showNetworkConnectionProblemsAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention!", nil)
                                                        message:NSLocalizedString(@"Bad network connection. Please, check your internet connection and try again later.", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - UIWebViewDelegate Methods

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self showActivityIndicator];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self hideActivityIndicator];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    BOOL tokenSaved = [self.model findAndSaveAccessTokenWithUrl:request.URL];
    if (tokenSaved) {
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf showMainController];
        });
    }
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self showActivityIndicator];
    if (error.code == InternetConnectionOfflineCode) {
        [self setNeedsDataReload:YES];
        [self showNetworkConnectionProblemsAlert];
    }
}

@end
