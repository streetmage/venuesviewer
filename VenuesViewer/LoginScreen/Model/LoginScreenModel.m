//
//  LoginScreenModel.m
//  VenuesViewer
//
//  Created by Admin on 22.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "LoginScreenModel.h"
#import "AppSession.h"

#import <RestKit/RestKit.h>

static NSString *const FoursquareAuthenticatePath = @"https://foursquare.com/oauth2/authenticate";
static NSString *const FoursquareClientId = @"MUASB4IOPYBRC32HKZL3IGANNM03CQ1BTTAI5HPSQQKUA0TI";
static NSString *const FoursquareResponseType = @"token";
static NSString *const FoursquareRedirectUri = @"http://localhost";

static NSUInteger const ExpectedResponseFragmentComponentsCount = 2;

@implementation LoginScreenModel

@synthesize authenticateRequest = _authenticateRequest;

- (NSURLRequest *)authenticateRequest {
    if (!_authenticateRequest) {
        NSMutableString *pathWithParameters = [[NSMutableString alloc] initWithString:FoursquareAuthenticatePath];
        [pathWithParameters appendString:[NSString stringWithFormat:@"?client_id=%@", FoursquareClientId]];
        [pathWithParameters appendString:[NSString stringWithFormat:@"&response_type=%@", FoursquareResponseType]];
        [pathWithParameters appendString:[NSString stringWithFormat:@"&redirect_uri=%@", FoursquareRedirectUri]];
        _authenticateRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:pathWithParameters]];
    }
    return _authenticateRequest;
}

- (BOOL)findAndSaveAccessTokenWithUrl:(NSURL *)url {
    
    if ([FoursquareRedirectUri containsString:url.host]) {
        
        NSString *fragment = url.fragment;
        NSArray *fragmentComponents = [fragment componentsSeparatedByString:@"="];
        
        if (fragmentComponents.count == ExpectedResponseFragmentComponentsCount &&
            [fragmentComponents.firstObject isEqualToString:@"access_token"]) {
            
            NSString *accessToken = fragmentComponents.lastObject;
            [[AppSession defaultSession] setAccessToken:accessToken];
            return YES;
            
        }
        
    }
    
    return NO;
    
}

@end
