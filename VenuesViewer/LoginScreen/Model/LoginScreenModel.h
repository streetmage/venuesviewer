//
//  LoginScreenModel.h
//  VenuesViewer
//
//  Created by Admin on 22.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginScreenModel : NSObject

@property (nonatomic, readonly) NSURLRequest *authenticateRequest;

// Returns YES if access token found in URL and saves it to local storage
- (BOOL)findAndSaveAccessTokenWithUrl:(NSURL *)url;

@end
