//
//  LoginScreenView.h
//  VenuesViewer
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginScreenView : UIView

@property (nonatomic, readonly) UIWebView *webView;

@end
