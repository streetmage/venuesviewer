//
//  LoginScreenView.m
//  VenuesViewer
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "LoginScreenView.h"

@implementation LoginScreenView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        _webView = [[UIWebView alloc] init];
        [self addSubview:_webView];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_webView setFrame:self.bounds];
}

@end
