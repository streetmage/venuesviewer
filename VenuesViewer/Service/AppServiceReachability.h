//
//  AppServiceReachability.h
//  VenuesViewer
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AppService.h"

extern NSString *const ServiceReachabilityChangeNotification;
extern NSString *const ServiceReachabilityStatusInfoKey;

typedef NS_ENUM(NSUInteger, ServiceReachabilityStatus) {
    ServiceUnreachable = 0,
    ServiceReachable = 1,
};

@interface AppService(AppServiceReachability)

@property (nonatomic, readonly) ServiceReachabilityStatus reachabilityStatus;

- (void)startPopulatingReachabilityNotifications;
- (void)stopPopulatingReachabilityNotification;

@end

