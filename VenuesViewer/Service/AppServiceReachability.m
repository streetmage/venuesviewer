//
//  AppServiceReachability.m
//  VenuesViewer
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AppServiceReachability.h"

#import <RestKit/RestKit.h>

NSString *const ServiceReachabilityChangeNotification = @"ServiceReachabilityChangeNotification";
NSString *const ServiceReachabilityStatusInfoKey = @"ServiceReachabilityStatusInfoKey";

@implementation AppService(AppServiceReachability)

#pragma mark - Accessors

- (ServiceReachabilityStatus)reachabilityStatus {
    AFNetworkReachabilityStatus status = _objectManager.HTTPClient.networkReachabilityStatus;
    BOOL serviceReachable = status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi;
    return serviceReachable ? ServiceReachable : ServiceUnreachable;
}

#pragma mark - Public Methods

- (void)startPopulatingReachabilityNotifications {
    [_objectManager.HTTPClient setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        BOOL serviceReachable = status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi;
        ServiceReachabilityStatus reachabilityStatus = serviceReachable ? ServiceReachable : ServiceUnreachable;
        NSDictionary *userInfo = @{ServiceReachabilityStatusInfoKey : @(reachabilityStatus)};
        NSNotification *notification = [NSNotification notificationWithName:ServiceReachabilityChangeNotification
                                                                     object:nil
                                                                   userInfo:userInfo];
        [[NSNotificationCenter defaultCenter] postNotification:notification];
    }];
}

- (void)stopPopulatingReachabilityNotification {
    [_objectManager.HTTPClient setReachabilityStatusChangeBlock:nil];
}

@end
