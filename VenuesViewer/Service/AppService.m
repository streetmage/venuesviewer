//
//  AppService.m
//  VenuesViewer
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AppService.h"
#import "AppSession.h"

#import "AllEntities.h"

#import <RestKit/RestKit.h>

#define NullCheck(obj) obj == nil ? [NSNull null] : obj

typedef void(^SuccessResponseHandler)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult);
typedef void(^FailureResponseHandler)(RKObjectRequestOperation *operation, NSError *error);

static NSString *const FoursquareHost = @"https://api.foursquare.com";
static NSString *const FoursquareAPIVersion = @"20150829";
static NSString *const FoursquareAPISearchPath = @"/v2/venues/search";

@interface AppService ()

@end

@implementation AppService

#pragma mark - Singleton Methods

+ (instancetype)sharedService {
    static AppService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[AppService alloc] init];
    });
    return service;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self setupRestKit];
    }
    
    return self;
}

#pragma mark - RestKit Methods

- (void)setupRestKit {
    
    _objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:FoursquareHost]];
    _objectManager.operationQueue.maxConcurrentOperationCount = 2;
    
    NSIndexSet *successfulCodesIndexSet = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    RKResponseDescriptor *searchResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[Venue objectMapping]
                                                                                                  method:RKRequestMethodGET
                                                                                             pathPattern:FoursquareAPISearchPath
                                                                                                 keyPath:@"response.venues"
                                                                                             statusCodes:successfulCodesIndexSet];
    [_objectManager addResponseDescriptor:searchResponseDescriptor];
    
}

- (void)getObjectsAtPath:(NSString *)path parameters:(NSDictionary *)parameters completion:(ServiceCompletionHandler)completion {
    NSMutableURLRequest *request = [_objectManager requestWithObject:nil
                                                              method:RKRequestMethodGET
                                                                path:path
                                                          parameters:parameters];
    SuccessResponseHandler success = ^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (completion) {
            completion(YES, mappingResult.array, operation.HTTPRequestOperation.responseString, nil);
        }
    };
    FailureResponseHandler failure = ^(RKObjectRequestOperation *operation, NSError *error) {
        if (completion) {
            completion(NO, nil, operation.HTTPRequestOperation.responseString, error);
        }
    };
    RKObjectRequestOperation *requestOperation = [_objectManager objectRequestOperationWithRequest:request
                                                                                           success:success
                                                                                           failure:failure];
    [_objectManager enqueueObjectRequestOperation:requestOperation];
}

#pragma mark - Public Methods

- (void)searchVenuesWithLatitude:(double)latitude
                       longitude:(double)longitude
                          radius:(NSUInteger)radius
                      completion:(ServiceCompletionHandler)completion {
    NSString *latitudeLongitude = [NSString stringWithFormat:@"%.02f,%.02f", latitude, longitude];
    NSMutableDictionary *parameters = [self requiredDefaultParameters];
    [parameters addEntriesFromDictionary:@{@"ll" : latitudeLongitude,
                                           @"intent" : @"browse",
                                           @"radius" : @(radius)}];
    [self getObjectsAtPath:FoursquareAPISearchPath
                parameters:parameters
                completion:completion];
}

- (void)cancelSearch {
    [_objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodAny
                                           matchingPathPattern:FoursquareAPISearchPath];
}

#pragma mark - Private Methods

- (NSMutableDictionary *)requiredDefaultParameters {
    NSArray *requiredValues = @[NullCheck([[AppSession defaultSession] accessToken]), FoursquareAPIVersion];
    NSArray *requiredKeys = @[@"oauth_token", @"v"];
    NSMutableDictionary *requiredParameters = [NSMutableDictionary dictionaryWithObjects:requiredValues forKeys:requiredKeys];
    return requiredParameters;
}

@end
